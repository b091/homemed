$(document).on('click', "#addToCart", function(){
    var _vm=$(this);
    var _quantity=$("#product_quantity").val();
    var _idProduct=$(".id-product").val();
    var _nameProduct=$(".name-product").val();
    var _priceProduct=$(".price-product").text();
    console.log(_quantity,_idProduct,_nameProduct);

    $.ajax({
         url:'/add-to-cart',
         data:{
             'id' :_idProduct,
             'qty' : _quantity,
             'name' : _nameProduct,
             'price' : _priceProduct
         },
         dataType:'json',
         beforeSend:function(){
            _vm.attr('disabled',true);
         },
         success:function(res){
            console.log(res);
            _vm.attr('disabled',false);
         }
    });
});
