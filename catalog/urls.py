from django.urls import path
from catalog.views import product, add_to_cart
urlpatterns = [
    path('', product, name='catalog'),
    path('add-to-cart/<int:id>', add_to_cart, name='add-to-cart'),
]