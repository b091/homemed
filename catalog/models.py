from django.db import models
from django.conf import settings

# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=200)
    specs = models.CharField(max_length=100)
    detail = models.TextField()
    price = models.PositiveIntegerField()
    id_product = models.PositiveIntegerField()
    image = models.ImageField(upload_to = 'images/')
    

    def __str__(self):
        return '{}. {}'.format(self.id, self.name)
