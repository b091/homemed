from django.http.response import HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.core import serializers
from .models import Cart
from catalog.models import Product
from django.shortcuts import (get_object_or_404, render, HttpResponseRedirect)
from django.views.decorators.csrf import csrf_exempt
# Create your views here
#@login_required(login_url='user/login')
def cart_list(request):
    # for item in request.session['cartdata'].items():
    #     item_cart = get_object_or_404(Product, id_product = item)
    #     new_profile = Cart(Barang=item_cart, jumlah=item['qty'])
    #     new_profile.save()

    totalRupiah = 0
    cart = Cart.objects.all()
    if cart.count != 0:
        for items in cart:
            totalRupiah +=  int(items.jumlah) * float(items.Barang.price)
        return render(request, 'index.html', {'cart_data' : Cart.objects.all(), 'totalitems': cart.count(), 
        'harga' : totalRupiah })
    else:
        return render(request, 'index.html', {'cart_data' : '', 'totalitems':0,'harga' : totalRupiah})

def delete_item(request, id):
    item = get_object_or_404(Product, id_product = id)
    lol = Cart.objects.filter(Barang = item)
    
    lol.delete()
    totalRupiah = 0
    for items in Cart.objects.all():
        totalRupiah +=  int(items.jumlah) * float(items.Barang.price)
    newCart = render_to_string('ajax/cart_list.html', {'cart_data' :Cart.objects.all(), 'totalitems': Cart.objects.all().count(), 
        'harga' : totalRupiah })

    
    return JsonResponse({'data' : newCart})

def update_item(request, id, jumlah):
    item = get_object_or_404(Product, id_product = id)
    lol = get_object_or_404(Cart, Barang = item)
    lol.jumlah = jumlah
    lol.save()
    totalRupiah = 0
    for items in Cart.objects.all():
        totalRupiah +=  int(items.jumlah) * float(items.Barang.price)
    newCart = render_to_string('ajax/cart_list.html', {'cart_data' :Cart.objects.all(), 'totalitems': Cart.objects.all().count(), 
        'harga' : totalRupiah })
    return JsonResponse({'data' : newCart})

def checkout(request):
    if Cart.objects.all().count() == 0:
        return JsonResponse ({'data' : "maaf anda tidak memiliki barang pada cart"})
    else:
        Cart.objects.all().delete()
        return JsonResponse({'data' : 'barang berhasil di checkout'})

    
def cart_list_flutter_prod(request):
    detail = serializers.serialize('json', Product.objects.all())
    return HttpResponse(detail, content_type = "application/json")

def cart_list_flutter_jlh(request):
    detail = serializers.serialize('json', Cart.objects.all())
    return HttpResponse(detail, content_type = "application/json")
@csrf_exempt
def update_item_flutter_kurang(request, id):

    item = get_object_or_404(Cart, Barang = id)   
    item.jumlah-=1
    print(item.jumlah)
    if item.jumlah == 0:
        lol = Cart.objects.filter(Barang = id)
        lol.delete()
        return JsonResponse()
    item.save()
    return JsonResponse()
@csrf_exempt
def update_item_flutter_tambah(request, id):
    item = get_object_or_404(Cart, Barang = id)   
    item.jumlah+=1
    item.save()
    return JsonResponse()
@csrf_exempt
def delete_item_flutter(request, id):
    lol = Cart.objects.filter(Barang = id)
    lol.delete()
    return JsonResponse()
def checkout_flutter(request):
    if Cart.objects.all().count() == 0:
        return JsonResponse ({'data' : "maaf anda tidak memiliki barang pada cart"})
    else:
        Cart.objects.all().delete()
        return JsonResponse({'data' : 'barang berhasil di checkout'})