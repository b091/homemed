from django.urls import path, re_path, include
from .views import *

urlpatterns = [
    # path('', views.index, name='index'),
    # path('add', views.add_friend),
    path('', cart_list),
    path('delete-from-cart<id>',delete_item),
    path('update-from-cart<id><jumlah>', update_item),
    path('checkout-cart', checkout),
    path('cart_list_flutter', cart_list_flutter_prod),
    path('cart_list_flutter_isi',cart_list_flutter_jlh),
    path('cart_list_update_tambah/<id>',update_item_flutter_tambah),
    path('cart_list_update_kurang/<id>',update_item_flutter_kurang),
    path('checkout_flutter', checkout_flutter),
    path('delete_flutter/<id>',delete_item_flutter)
    # TODO Add friends path using friend_list Views
]