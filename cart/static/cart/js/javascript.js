$(document).ready(function(){
    $(document).on('click','.delete-item',function(){
		var items=$(this).attr('data-item');
		var objek=$(this);
		// Ajax
		$.ajax({
			url:'/cart/delete-from-cart'+items,
			beforeSend:function(){
				objek.attr('disabled',true);
			},
			success:function(res){
				objek.attr('disabled',false);
				$(".page").html(res.data);
			}
		});
		// End
	});
    $(document).on('click','.update-item',function(){
		var items=$(this).attr('data-item');
        var jumlah = $('#quantity-'+items).val();
		var objek=$(this);
		// Ajax
		$.ajax({
			url:'/cart/update-from-cart'+items+jumlah,
			beforeSend:function(){
				objek.attr('disabled',true);
			},
			success:function(res){
				objek.attr('disabled',false);
				$(".page").html(res.data);
			}
		});
		// End
	});
    $(document).on('click','.btn-block',function(){
        var objek = $(this);
        $.ajax({
            url : '/cart/checkout-cart',
            dataType : 'json',
            beforeSend:function(){
                objek.attr('disabled', true)
            },
            success:function(res){
                objek.attr('disabled', false)
				alert(res.data);
                $(".page").html({'cart_data' :"", 'totalitems': 0, 
                'harga' : 0 })
            }
        })
    })
});
