from django import forms
from . import models

class NoteCart(forms.ModelForm):
    class Meta:
        model = models.Cart
        fields = "__all__"