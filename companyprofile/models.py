from django.db import models

# Create your models here.
class Contact(models.Model):
    Email = models.CharField(max_length= 50)
    Message = models.TextField()
