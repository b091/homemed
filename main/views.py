from django.http.response import HttpResponseRedirect
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from .models import join
from .forms import JoinForm
from django.core import serializers
from django.http import JsonResponse

def home(request):
    # print("Test 1")
    form = JoinForm()
    return render(request, 'main/home.html', {'form': form})
    

def add_join(request):
    print("sukes 1")
    if request.method == 'POST':
        print("sukses 1")
        form = JoinForm(request.POST)
        if form.is_valid():
            print("sukses 2")
            form.save()
            return redirect('main:home')
    else:
        form = JoinForm()
    return render(request, 'home.html', {'form': form})

def json(request):
    data = serializers.serialize('json' , join.objects.all())
    return HttpResponse(data, content_type = "application/json")
