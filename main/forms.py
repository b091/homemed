from .models import join
from django import forms
from django.forms import fields

class JoinForm(forms.ModelForm):
    class Meta:
        model = join
        fields = "__all__"