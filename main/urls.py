from django.urls import path
from django.urls.conf import include

# from . import views
from .views import add_join,json, home

app_name = 'main'

urlpatterns = [
    # path('', views.home, name='home'),
    path('user/', include('user.urls'), name='login'),
    path('', home, name='home'),
    path('add_join', add_join, name='add_join'),
    path('json', json, name='json')
]
