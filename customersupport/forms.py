from django import forms
from django.db.models import fields
from .models import subscribe

class SubscribeForm(forms.ModelForm):
    class Meta:
        model = subscribe
        fields = "__all__"