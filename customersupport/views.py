from django.shortcuts import render, redirect
from .models import subscribe
from .forms import SubscribeForm
from django.core import serializers
from django.http.response import HttpResponse
from django.http import JsonResponse 
# from django.template import loader

# Create your views here.
def index(request):
    return render(request, 'customersupport.html')

# def lihat_data(request):
#     data = subscribe.objects.all().values()
#     response = {'data': data}
#     return render(request, "lihatdata.html", response)

def create(request, nama, email):
    # if request.method == 'POST':
    #     nama = request.POST['nama']
    #     email = request.POST['email']
    new_profile = subscribe(nama=nama,email=email)
    # print("LOL")
    new_profile.save()
    success = {"nama" : nama, "email" : email}
    # print(nama)
    # print(email)  
    return JsonResponse({"kirim":success})
        # success = nama + ' dengan email' + email + ' terdaftar'
    # success = serializers.serialize('json', new_profile)
    # return HttpResponse(success, content_type="application/json")

def add_subscribe(request):
    context ={}
  
    # create object of form
    form = SubscribeForm(request.POST or None, request.FILES or None)
    if request.method == "POST":  
    # check if form data is valid
        if form.is_valid():
            # save the form data to model
            form.save()

    context['form']= form
    return render(request, "customersupport.html", context)

def json(request):
    data = serializers.serialize('json', subscribe.objects.all())
    return HttpResponse(data, content_type="application/json")
    

# def get(self, request):
#     if request.is_ajax():
#         template = loader.get_template