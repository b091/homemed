from django.urls import path
from . import views
from .views import add_subscribe, json, index

urlpatterns = [
    path('', index, name='customersupport'),
    path('json', json, name='json'),
    # path('create<nama><email>', views.create, name='create'),
    path('create/<str:nama>/<str:email>', views.create, name='create'),
    # path('lihatdata', lihat_data, name='lihatdata'),
]
