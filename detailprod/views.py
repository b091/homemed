from django.http.response import JsonResponse
from django.shortcuts import render
from catalog.models import Product
# from chris.models import Cart
from django.contrib.auth.models import User
from .models import Review
from .forms import ReviewForm
from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required
# import json
# from django.db.models import Max,Min,Count,Avg

def index(request):
    return render(request, 'productDetail.html')

# @login_required(login_url='user/login')
def productDetail(request,id):
    # print("hahah")
    product = get_object_or_404(Product, id_product=id) #makesure brasel ada idnya
    canAdd=True
	
    reviewCheck = Review.objects.filter(user=request.user,id_product=product).count()
    if request.user.is_authenticated:
        if reviewCheck > 0:
            canAdd=False
    
    review = Review.objects.filter(id_product=product)

    # print(product.name)

    reviewForm = ReviewForm(request.POST or None)
    if (request.method == "POST"):
        reviewForm = reviewForm(request.POST)
        if (reviewForm.is_valid()):
            reviewForm.save()

    context = {
        'product' : product,
        'reviewForm' : reviewForm,
        'review' : review,
        'canAdd':canAdd,
    }

    # print("aaa")
    return render(request, 'productDetail.html', context)

def addReview(request, id, nama, review):
    print(nama)
    print(review)
    print(id)
    response={}
    namaRev = nama
    strReview = review
    product = get_object_or_404(Product, id_product=id)

    # product = Product.objects.get(id=id)
    # if (request.method =='POST'):
        
    # strName = namaRev
    # prodId = product.id_product
    # strInput = strReview
    
    # print (namaRev + "huhu")
    if ( strReview != ""):
        newReview = Review.objects.create(
            review_txt = strReview,
            id_product = product,
            user = request.user,)
        newReview.save() 
        print(" hihi")
    # response['message'] = strName
    response = {"strName" : namaRev, "strReview" : review}
    # print(nama)
    # print(email)  
    return JsonResponse({"kirim":response})

    # data = serializers.serialize('json', Review.objects.all())
    # return HttpResponse(data, content_type="application/json")

def json(request):
    data = serializers.serialize('json', Review.objects.all())
    return HttpResponse(data, content_type="application/json")
