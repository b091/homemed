from django.urls import path

from . import views 
from .views import productDetail, addReview, index, json


# app_name = 'detail'

urlpatterns = [
    path('', index, name='productDetail'),
    path('<int:id>', productDetail, name = 'details'),
    path('addReview/<int:id>/<str:nama>/<str:review>',addReview, name='addReview'),
    path('json', json, name='json'),
]
