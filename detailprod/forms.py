from django import forms
from .models import Review

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ('review_txt','id_product','user')
        widgets = {
            'review': forms.Textarea(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Berikan ulasan mengenai barang'
                }
            ),
        }

