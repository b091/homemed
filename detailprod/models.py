from django.db import models
from catalog.models import Product  #nanti import dari modelsnya brasel
from django.conf import settings
from django.contrib.auth.models import User


# Create your models here.
# RATING=(
#     (1,'1'),
#     (2,'2'),
#     (3,'3'),
#     (4,'4'),
#     (5,'5'),
# )

class Review(models.Model):
    review_txt = models.TextField()
    id_product =models.ForeignKey(Product,on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE) #mending pake ini atau lgsg user
    # review_rating=models.CharField(choices=RATING,max_length=150)

    class Meta:
        verbose_name_plural='Reviews'

    # def get_review_rating(self):
    #     return self.review_rating

    def __str__(self):
        return self.review_txt


