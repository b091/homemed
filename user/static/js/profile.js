$(document).ready(() => {
    const username = $('#username');
    const email = $('#email');
    const address = $('#address');
    const phone = $('#phone');


    $.ajax({
        url: 'json',
        success: function (hasil) {
            const result = hasil[0].fields;
        console.log(result);
        username.text(result.username);
        email.text(result.email);
        address.text(result.address);
        phone.text(result.phone);
        },
    });
});