from django.forms import ModelForm
from django.db.models import fields
from .models import userInfo
from django.contrib.auth.forms import UserCreationForm
# from django.contrib.auth.models import User

class UserForm(UserCreationForm):
    class Meta :
        model = userInfo
        fields = ['username','email', 'password1', 'password2', 'address', 'phone']

class editProfile(ModelForm):
    class Meta :
        model = userInfo
        fields = ['username', 'password', 'email', 'address', 'phone']