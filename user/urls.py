from django.urls import path
from .views import currentUser, registerPage, index, loginPage, currentUser, profile
from django.contrib.auth import views

app_name = 'user'

urlpatterns = [
    path('', index, name='home'),
    path('register', registerPage, name='registerPage'),
    path('login', loginPage, name='login'),
    path('profile', profile, name='profile'),
    path('json', currentUser, name='edit-json'),
]