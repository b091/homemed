# Create your views here.
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse
from django.http.response import JsonResponse
from django.shortcuts import render, redirect, resolve_url, get_object_or_404
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.core import serializers

from django.contrib.auth import login as django_login, authenticate as django_authenticate ,logout as django_logout

from .forms import UserForm
from .models import userInfo

def index(request):
    home = userInfo.objects.all()
    return render(request, 'main/home.html')

@login_required(login_url='./login')
def profile(request):
    return render(request, 'account.html')

def currentUser(request):
    # print(request.user)
    
    user = get_object_or_404(userInfo, username=request.user.username)
    user = serializers.serialize('json', userInfo.objects.filter(username=request.user.username))
    # print(user)
    return HttpResponse(user, content_type="application/json")

def registerPage(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        form = UserForm()
        if request.method == 'POST':
            form = UserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
            return redirect('./login')

    context = {'form':form}
    return render(request, 'register.html', context)

def loginPage(request):
    if request.method == 'POST':
        # form = AuthenticationForm(request.POST)

        # if form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = django_authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    django_login(request,user)
                return redirect('/#/')
    context = {}
    return render(request, 'login.html', context)   

def logoutPage(request):
    django_logout(request)
    return redirect("./login")
